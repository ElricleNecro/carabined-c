CC=clang
CFLAGS=-std=c11 -W -Wall -Wextra -D_POSIX_C_SOURCE -Iinclude
LDFLAGS=-W -Wall -Wextra

.PHONY: all

all: build build/vm

build:
	@mkdir build

build/%.o: src/%.c
	$(CC) $(CFLAGS) -c $^ -o $@

build/vm: build/main.o build/stack.o build/instructions.o
	$(CC) $(LDFLAGS) $^ -o $@
