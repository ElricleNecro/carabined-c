#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "stack.h"
#include "instructions.h"

typedef struct register_t {
	uint8_t *ip;	// Instruction pointer
	uint8_t  fp;	// Frame pointer
} Register;

void usage(void) {
	printf("Usage: vm <usage>");
}

uint8_t* load_file(const char *fname) {
	FILE *f = NULL;
	uint8_t *code = NULL;
	struct stat st;

	if( (f = fopen(fname, "r")) == NULL ) {
		fprintf(stderr, "ERROR: cannot open file %s\n", fname);
		usage();
		return NULL;
	}

	// Extracting some statistics about the file:
	fstat(fileno(f), &st);
	code = malloc(st.st_size);
	fread((void*)code, 1, st.st_size, f);

	fclose(f);

	return code;
}

int main(int argc, char *argv[]) {
	if( argc != 2 ) {
		usage();
		return EXIT_FAILURE;
	}

	uint8_t *ip = NULL;
	uint8_t *code = NULL;
	if( ( ip = code = load_file(argv[1]) ) == NULL ) {
		usage();
		return EXIT_FAILURE;
	}
	Stack data = stack_new(1024);
	instruction ops[256] = { op_nop };

	ops['c'] = op_push_char;
	ops['e'] = op_emit;

	while( *ip != 'h' ) {
		ip = ops[ *ip ](ip, &data);
	}

	stack_free(&data);
	free(code);

	return EXIT_SUCCESS;
}
