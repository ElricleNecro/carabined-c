#include "instructions.h"

uint8_t* op_nop(uint8_t *ip, Stack *s) {
	return ip + 1;
	(void)s;
}

uint8_t* op_push_char(uint8_t *ip, Stack *s) {
	Object o = {.type = 'c'};
	o.u8     = *(ip + 1);

	stack_push(s, o);

	return ip + 2;
}

uint8_t* op_emit(uint8_t *ip, Stack *s) {
	Object o = stack_pop(s);

	putchar(o.u8);

	return ip + 1;
}
