#include "stack.h"

Stack stack_new(unsigned int size) {
	Stack obj = {.top = 0, .size = size};

	obj.stack = malloc(size * sizeof(struct object_t));

	return obj;
}

void stack_free(Stack *s) {
	free(s->stack);
	s->size = s->top = 0;
}

unsigned int stack_push(Stack *s, Object o) {
	s->stack[ s->top++ ] = o;
	return s->top;
}

Object stack_pop(Stack *s) {
	return s->stack[ --(s->top) ];
}

Object stack_peek(Stack *s) {
	return s->stack[ s->top - 1 ];
}
