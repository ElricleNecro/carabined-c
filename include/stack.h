#ifndef STACK_H_WU1QID9C
#define STACK_H_WU1QID9C

#include <stdint.h>
#include <stdlib.h>

typedef struct object_t {
	uint8_t type;

	union {
		uint8_t  u8;
		int8_t   i8;
		uint32_t u32;
		int32_t  i32;
		void    *ptr;
	};
} Object;

typedef struct stack_t {
	unsigned int top;
	unsigned int size;

	struct object_t *stack;
} Stack;

Stack stack_new(unsigned int size);

void stack_free(Stack *s);

unsigned int stack_push(Stack *s, Object o);

Object stack_pop(Stack *s);

Object stack_peek(Stack *s);

#endif /* end of include guard: STACK_H_WU1QID9C */
