#ifndef INSTRUCTIONS_H_6OBTOR7U
#define INSTRUCTIONS_H_6OBTOR7U

#include <stdint.h>
#include <stdio.h>

#include "stack.h"

typedef uint8_t* (*instruction)(uint8_t*, Stack*);

uint8_t* op_nop(uint8_t *ip, Stack *s);

uint8_t* op_push_char(uint8_t *ip, Stack *s);

uint8_t* op_emit(uint8_t *ip, Stack *s);

#endif /* end of include guard: INSTRUCTIONS_H_6OBTOR7U */
